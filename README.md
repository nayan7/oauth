# Django + React Social Authentication

```
cd drf_social
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

This should start the django development server at `localhost:8000`

### Frontend
```
cd react_auth	
npm install 
npm start
```

