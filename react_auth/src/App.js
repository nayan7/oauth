import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
import googleLogin from "./services/googleLogin"
import './App.css';

class App extends Component {

  render() {

    const responseGoogle = async(response) => {
      let googleResponse  = await googleLogin(response.accessToken)
      console.log(googleResponse);
      console.log(response);
    }

    return (
      <div className="App">
        <h1>giftperf</h1>


        <GoogleLogin
          clientId='1040906393010-cdnpiitkcv3jvnm2o1jfatq4p0llf1qu.apps.googleusercontent.com'
          buttonText="LOGIN WITH GOOGLE"
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
        />

      </div>
    );
  }
}

export default App;
